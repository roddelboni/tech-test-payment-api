﻿using AutoMapper;
using Pottencial.Sales.Application.UseCases.InsertSale;
using Pottencial.Sales.Domain.Entities;
using Pottencial.Sales.Domain.Interfaces.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Sales.Unit.Tests.UseCases.GetSales
{
    [TestFixture]
    public class SaleHandlerTests
    {
        private readonly SaleHandler _saleHandler;
        private readonly Mock<ISalesRepository> _saleRepositoryMock;
        private readonly Mock<IMapper> _mapperMock;

        public SaleHandlerTests()
        {
            _saleRepositoryMock = new Mock<ISalesRepository>();
            _mapperMock = new Mock<IMapper>();

            _saleHandler = new SaleHandler(_saleRepositoryMock.Object, _mapperMock.Object);
        }

        [Test]
        public async Task Handle_InsertsValid_Sale_ReturnsTrue()
        {
            // Arrange
            var request = new UpdateSaleInput();
            var entity = new Sale();

            _mapperMock.Setup(x => x.Map<Sale>(request)).Returns(entity);

            _saleRepositoryMock.Setup(x => x.Save(entity, It.IsAny<CancellationToken>())).Returns(Task.FromResult(true));

            // Act
            var result = await _saleHandler.Handle(request, It.IsAny<CancellationToken>());

            // Assert
            Assert.IsTrue(result);
            _mapperMock.Verify(x => x.Map<Sale>(request));
            _saleRepositoryMock.Verify(x => x.Save(entity, It.IsAny<CancellationToken>()));
        }

        [Test]
        public async Task Handle_Inserts_Invalid_Sale_ThrowsException()
        {
            // Arrange
            var request = new UpdateSaleInput();
            var entity = new Sale();           
            _mapperMock.Setup(x => x.Map<Sale>(request)).Returns(entity);
            _saleRepositoryMock.Setup(x => x.Save(entity, It.IsAny<CancellationToken>())).Throws(new Exception());


            // Act & Assert
            Assert.ThrowsAsync<Exception>(async () => await _saleHandler.Handle(request, It.IsAny<CancellationToken>()));
            _mapperMock.Verify(x => x.Map<Sale>(request));
            _saleRepositoryMock.Verify(x => x.Save(entity, It.IsAny<CancellationToken>()));
        }
    }
}
