﻿using System.ComponentModel;

namespace Pottencial.Sales.Domain.Enums
{
    public enum EStatus
    {
        [Description("Aguardando pagamento")]
        AguardandoPagamento = 0,

        [Description("Pagamento Aprovado")]
        PagamentoAprovado = 1,

        [Description("Enviado para transportadora")]
        EnviadoParaTransportadora = 2,

        [Description("Entregue")]
        Entregue = 3,

        [Description("Cancelada")]
        Cancelada = 4

    }
}
