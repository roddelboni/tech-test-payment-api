﻿using Pottencial.Sales.Domain.Entities;

namespace Pottencial.Sales.Domain.Interfaces.Repositories
{
    public interface ISalesRepository
    {
        public Task<Sale> GetById(long id, CancellationToken cancellationToken);
        public Task<bool> Save(Sale sale, CancellationToken cancellationToken);

        public Task<bool> Update(Sale sale, CancellationToken cancellationToken);
    }
}
