﻿using Pottencial.Sales.Domain.Enums;

namespace Pottencial.Sales.Domain.Interfaces.Services
{
    public interface IStatusChange
    {
        public bool CheckChangedStatus(EStatus Id, EStatus @status, CancellationToken cancellation);
    }
        
}
