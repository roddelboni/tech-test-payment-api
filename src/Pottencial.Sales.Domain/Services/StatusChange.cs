﻿using Pottencial.Sales.Domain.Enums;
using Pottencial.Sales.Domain.Interfaces.Repositories;
using Pottencial.Sales.Domain.Interfaces.Services;
using Pottencial.Sales.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Sales.Domain.Services
{
    public class StatusChange : StatusTypeList, IStatusChange
    {
        private readonly ISalesRepository _salesRepository;
        private bool statusToChange { get; set; } = false;

        public StatusChange() {}

        public bool CheckChangedStatus(EStatus statusTo, EStatus StatusFrom, CancellationToken cancellation)
        {         

            if (StatusFrom.Equals(EStatus.AguardandoPagamento))
            {
                return statusToChange = EStatusListWait.Contains(statusTo);
            }
            
            if (StatusFrom.Equals(EStatus.PagamentoAprovado))
            {
                return statusToChange = EStatusListSend.Contains(statusTo);
            }

            if (StatusFrom.Equals(EStatus.EnviadoParaTransportadora))
            {
                return statusToChange = EStatusListSend.Contains(statusTo);
            }

            return statusToChange;
        }
    }
}
