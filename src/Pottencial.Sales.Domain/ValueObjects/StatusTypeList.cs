﻿using Pottencial.Sales.Domain.Enums;

namespace Pottencial.Sales.Domain.ValueObjects
{
    public class StatusTypeList
    {
        internal List<EStatus> EStatusListWait = new List<EStatus>
            { EStatus.PagamentoAprovado, EStatus.Cancelada};


        internal List<EStatus> EStatusListApproved = new List<EStatus>
            { EStatus.EnviadoParaTransportadora, EStatus.Cancelada};

        internal List<EStatus> EStatusListSend = new List<EStatus>
            { EStatus.Entregue};
    }
}
