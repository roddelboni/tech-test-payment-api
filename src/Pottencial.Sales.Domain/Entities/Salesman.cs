﻿namespace Pottencial.Sales.Domain.Entities
{
    public class Salesman
    {        
        public long Id { get; set; }
        public string CPF { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public bool PhoneNumber { get; set; }
    }
}
