﻿using Pottencial.Sales.Domain.Enums;

namespace Pottencial.Sales.Domain.Entities
{
    public class Sale
    {
        public long Id { get; set; }
        public Guid Identifier { get; set; }
        public DateTime SalesDate { get; set; }
        public EStatus StatusSale { get; set; }
        public List<Item> Items { get; set; }
        public Salesman Salesman { get; set; }

    }
}
