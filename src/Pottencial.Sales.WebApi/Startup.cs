﻿using AutoMapper;
using Pottencial.Sales.Infra.Data.Sql.DependencyInjection;
using Pottencial.Sales.Application.DependencyInjection;
using Pottencial.Sales.Application.Mapper;
using MediatR;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace Pottencial.Sales.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            var configMap = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<SalesProfile>();                
            });

            var mapper = configMap.CreateMapper();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddSwaggerGen();
            services.AddControllers();
            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
            services.AddSqlServer();
            services.AddUseCasesInjection();
            
        }

        public void ConfigureSwagger(IServiceCollection services)
        {           
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Pottencial.Sales", Version = "v1" });               
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pottencial.Sales V1");                
            });                        

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
