﻿using Pottencial.Sales.Application.UseCases.GetSales;
using Pottencial.Sales.Application.UseCases.InsertSale;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Pottencial.Sales.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SalesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SalesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetSalesOutput))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(bool))]
        public async Task<ActionResult<GetSalesOutput>> GetById(long id, CancellationToken cancellation)
        {
            var input = new GetSalesInput()
            {
                Id = id
            };

            var result = await _mediator.Send(input, cancellation);

            return Ok(result);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(bool))]
        public async Task<ActionResult<bool>> InsertSale([FromBody] UpdateSaleInput input, CancellationToken cancellation)
        {
            var result = await _mediator.Send(input, cancellation);

            return Ok(result);
        }

        [HttpPatch]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(bool))]
        public async Task<ActionResult<bool>> UpdateSale([FromBody] UpdateSaleInput input, CancellationToken cancellation)
        {
            var result = await _mediator.Send(input, cancellation);

            return Ok(result);
        }
    }
}
