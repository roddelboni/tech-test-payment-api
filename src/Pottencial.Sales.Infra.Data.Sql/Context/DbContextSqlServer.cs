﻿using System.Data;
using System.Data.SqlClient;

namespace Pottencial.Sales.Infra.Data.Sql.Context
{
    public class DbContextSqlServer : IDisposable
    {
        private IDbConnection Connection { get; set; }

        public IDbConnection BuilConnection(string connectionString)
        {
            Connection = new SqlConnection(connectionString);
            Connection.Open();

            return Connection;
        }

        public void Dispose()
        {
            Connection?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
