﻿using Dapper;
using Pottencial.Sales.Infra.Data.Sql.Context;
using Microsoft.Extensions.Configuration;
using Pottencial.Sales.Data.Sql.Repositories.Sales;
using Pottencial.Sales.Domain.Entities;
using Pottencial.Sales.Domain.Interfaces.Repositories;

namespace Pottencial.Sales.Infra.Data.Sql.Repositories.Sales
{
    public class SalesRepository : ISalesRepository
    {
        private readonly DbContextSqlServer _context;
        private readonly string _connString;
        private const string SqlServerDbContext = "SqlServerDbContext";

        public SalesRepository(DbContextSqlServer context, IConfiguration configuration)
        {
            _context = context;
            _connString = configuration.GetConnectionString(SqlServerDbContext);
        }

        public async Task<Sale> GetById(long id, CancellationToken cancellationToken)
        {
            using var connection = _context.BuilConnection(_connString);

            var result = await connection.QuerySingleOrDefaultAsync<Sale>(
                new CommandDefinition(
                    SalesQueries.SelectByCpf, new { ID = id },
                    cancellationToken: cancellationToken));

            return result;

        }

        public async Task<bool> Save(Sale sale, CancellationToken cancellationToken)
        {
            using var connection = _context.BuilConnection(_connString);
            var objectToInsert = new
            {
                id = sale.Id.ToString(),
                Identifier = sale.Identifier.ToString(),
                Itens = sale.Items,
                vendedor = sale.Salesman
            };

            var result = connection.Execute(
                new CommandDefinition(
                    SalesQueries.Save, objectToInsert,                                       
                    cancellationToken: cancellationToken));

            var sucess = result > 0;

            return sucess;   

        }

        public async Task<bool> Update(Sale sale, CancellationToken cancellationToken)
        {
            using var connection = _context.BuilConnection(_connString);
            var objectToInsert = new
            {
                id = sale.Id.ToString(),
                Identifier = sale.Identifier.ToString(),
                Itens = sale.Items,
                vendedor = sale.Salesman
            };

            var result = connection.Execute(
                new CommandDefinition(
                    SalesQueries.Update, objectToInsert,
                    cancellationToken: cancellationToken));

            var sucess = result > 0;

            return sucess;
        }
    }
}
