﻿namespace Pottencial.Sales.Data.Sql.Repositories.Sales
{
    public static class SalesQueries
    {
        internal const string SelectByCpf = @"
                SELECT * 
                    FROM SALES S
                INNER JOIN 
                        ITEM I ON I.ID_SALES = S.ID
                INNER JOIN 
                        SALESMAN SL ON SL.ID_SALES = S.ID
                WHERE SL.CPF = @cpf";

        internal const string Save = @"
               ";

        internal const string Update = @"
               ";
    }
}
