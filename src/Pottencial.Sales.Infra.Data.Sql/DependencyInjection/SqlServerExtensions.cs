﻿using Pottencial.Sales.Infra.Data.Sql.Context;
using Pottencial.Sales.Domain.Interfaces.Repositories;
using Pottencial.Sales.Infra.Data.Sql.Repositories.Sales;
using Microsoft.Extensions.DependencyInjection;

namespace Pottencial.Sales.Infra.Data.Sql.DependencyInjection
{
    public static class SqlServerExtensions
    {
        public static IServiceCollection AddSqlServer(this IServiceCollection services)
        {
            services.AddTransient<DbContextSqlServer>();
            services.AddTransient<ISalesRepository, SalesRepository>();

            return services;
        }
    }
}
