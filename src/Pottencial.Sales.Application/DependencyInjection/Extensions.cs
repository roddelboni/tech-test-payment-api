﻿using Pottencial.Sales.Application.UseCases.GetSales;
using Microsoft.Extensions.DependencyInjection;

namespace Pottencial.Sales.Application.DependencyInjection
{
    public static class Extensions
    {
        public static IServiceCollection AddUseCasesInjection(this IServiceCollection services)
        {            
            services.AddTransient<IGetSalesHandler, GetSalesHandler>();
            return services;
        }
    }
}
