﻿using MediatR;

namespace Pottencial.Sales.Application.UseCases.GetSales
{
    public interface IGetSalesHandler : IRequestHandler<GetSalesInput, GetSalesOutput>
    {
    }
}
