﻿using AutoMapper;
using Pottencial.Sales.Application.UseCases.GetSales.Validation;
using Pottencial.Sales.Domain.Interfaces.Repositories;

namespace Pottencial.Sales.Application.UseCases.GetSales
{
    public class GetSalesHandler : IGetSalesHandler
    {
        private readonly ISalesRepository _salesRepository;
        private readonly IMapper _mapper;

        public GetSalesHandler(ISalesRepository salesRepository, IMapper mapper)
        {
            _salesRepository = salesRepository;
            _mapper = mapper;
        }

        public async Task<GetSalesOutput> Handle(GetSalesInput request, CancellationToken cancellationToken)
        {
            //log
            var validator = new GetSalesValidator();
            var validationResult = validator.Validate(request);

            if (validationResult != null)
            {
                //log
                var result = await _salesRepository.GetById(request.Id, cancellationToken);

                var output = _mapper.Map<GetSalesOutput>(result);
                return output;
            }

            return default;            
        }
    }
}
