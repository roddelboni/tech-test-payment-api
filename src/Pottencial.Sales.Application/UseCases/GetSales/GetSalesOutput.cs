﻿using Pottencial.Sales.Domain.Entities;

namespace Pottencial.Sales.Application.UseCases.GetSales
{
    public class GetSalesOutput
    {
        public Sale Sales { get; set; }
    }
}
