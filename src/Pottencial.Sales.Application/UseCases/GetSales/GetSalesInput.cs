﻿using MediatR;

namespace Pottencial.Sales.Application.UseCases.GetSales
{
    public class GetSalesInput : IRequest<GetSalesOutput>
    {
        public long Id { get; set; }         
    }
}
