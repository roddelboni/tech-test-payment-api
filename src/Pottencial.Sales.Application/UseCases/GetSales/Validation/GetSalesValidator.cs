﻿using FluentValidation;

namespace Pottencial.Sales.Application.UseCases.GetSales.Validation
{
    public class GetSalesValidator : AbstractValidator<GetSalesInput>
    {
        public GetSalesValidator()
        {
            RuleFor(sale => sale.Id).NotNull();
        }
    }
}
