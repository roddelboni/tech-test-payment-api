﻿using Pottencial.Sales.Domain.Interfaces.Repositories;
using FluentValidation;

namespace Pottencial.Sales.Application.UseCases.InsertSale.Validation
{
    public class InsertSaleValidation : AbstractValidator<InsertSaleInput>
    {
        public InsertSaleValidation(ISalesRepository saleRepository)
        {
            RuleFor(salesman => salesman.Salesman.CPF).NotNull();
        }
    }
}
