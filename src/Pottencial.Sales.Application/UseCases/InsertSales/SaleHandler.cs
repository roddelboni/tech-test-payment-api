﻿using AutoMapper;
using Pottencial.Sales.Application.UseCases.InsertSale.Validation;
using Pottencial.Sales.Domain.Entities;
using Pottencial.Sales.Domain.Enums;
using Pottencial.Sales.Domain.Interfaces.Repositories;

namespace Pottencial.Sales.Application.UseCases.InsertSale
{
    public class SaleHandler : ISaleHandler
    {
        private readonly ISalesRepository _salesRepository;
        private readonly IMapper _mapper;

        public SaleHandler(ISalesRepository salesRepository, IMapper mapper)
        {
            _salesRepository = salesRepository;
            _mapper = mapper; 
        }

        public async Task<bool> Handle(InsertSaleInput request, CancellationToken cancellationToken)
        {
            try
            {
                //log
                var validator = new InsertSaleValidation(_salesRepository);
                var validationResult = validator.Validate(request);

                if (validationResult.IsValid)
                {
                    //log
                    var entity = _mapper.Map<Sale>(request);
                    entity.StatusSale = EStatus.AguardandoPagamento;

                    var result = await _salesRepository.Save(entity, cancellationToken);

                    return result;
                }

                return validationResult.IsValid;

            }catch(Exception ex)
            {
                throw ex;
            }
            
        }
    }
}
