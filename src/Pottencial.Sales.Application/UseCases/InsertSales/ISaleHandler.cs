﻿using MediatR;

namespace Pottencial.Sales.Application.UseCases.InsertSale
{
    public interface ISaleHandler : IRequestHandler<InsertSaleInput, bool>
    {
    }
}
