﻿using AutoMapper;
using Pottencial.Sales.Application.UseCases.UpdateSale.Validation;
using Pottencial.Sales.Domain.Entities;
using Pottencial.Sales.Domain.Interfaces.Repositories;
using Pottencial.Sales.Domain.Interfaces.Services;
using static Dapper.SqlMapper;

namespace Pottencial.Sales.Application.UseCases.UpdateSale
{
    public class UpdateSaleHandler : IUpdateSaleHandler
    {
        private readonly ISalesRepository _salesRepository;
        private readonly IMapper _mapper;
        private readonly IStatusChange _statusChange;

        public UpdateSaleHandler(ISalesRepository salesRepository, IMapper mapper, IStatusChange statusChange)
        {
            _salesRepository = salesRepository;
            _mapper = mapper;
            _statusChange = statusChange;
        }

        public async Task<bool> Handle(UpdateSaleInput request, CancellationToken cancellationToken)
        {
            try
            {
                //log
                var validator = new UpdateSaleValidation(_salesRepository);
                var validationResult = validator.Validate(request);

                if (validationResult.IsValid)
                {
                    //log
                    var salesUpdate = await _salesRepository.GetById(request.Id, cancellationToken);

                    var entity = _mapper.Map<Sale>(request);

                    if (_statusChange.CheckChangedStatus(request.StatusSale, salesUpdate.StatusSale, cancellationToken))
                    {
                        return false;
                    }

                    var result = await _salesRepository.Update(entity, cancellationToken);

                    return result;
                }

                return validationResult.IsValid;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
