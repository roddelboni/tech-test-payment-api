﻿using MediatR;

namespace Pottencial.Sales.Application.UseCases.UpdateSale
{
    public interface IUpdateSaleHandler : IRequestHandler<UpdateSaleInput, bool>
    {
    }
}
