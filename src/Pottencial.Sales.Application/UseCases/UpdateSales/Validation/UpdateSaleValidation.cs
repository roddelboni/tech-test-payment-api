﻿using Pottencial.Sales.Domain.Interfaces.Repositories;
using FluentValidation;

namespace Pottencial.Sales.Application.UseCases.UpdateSale.Validation
{
    public class UpdateSaleValidation : AbstractValidator<UpdateSaleInput>
    {
        public UpdateSaleValidation(ISalesRepository saleRepository)
        {
            RuleFor(salesman => salesman.Salesman.CPF).NotNull();
        }
    }
}
