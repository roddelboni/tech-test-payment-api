﻿using Pottencial.Sales.Domain.Enums;
using MediatR;
using Pottencial.Sales.Domain.Entities;

namespace Pottencial.Sales.Application.UseCases.UpdateSale
{
    public class UpdateSaleInput : IRequest<bool>
    {
        public long Id { get; set; }
        public Guid Identifier { get; set; }
        public DateTime SalesDate { get; set; }
        public EStatus StatusSale { get; set; }
        public List<Item> Items { get; set; }
        public Salesman Salesman { get; set; }
    }
}
