﻿using AutoMapper;
using Pottencial.Sales.Application.UseCases.GetSales;
using Pottencial.Sales.Application.UseCases.InsertSale;
using Pottencial.Sales.Domain.Entities;

namespace Pottencial.Sales.Application.Mapper
{
    public class SalesProfile : Profile
    {
        public SalesProfile()
        {
            CreateMap<Sale, GetSalesOutput>();

            CreateMap< UpdateSaleInput, Sale>()                 
                .ReverseMap();
        }
    }
}
